const memo = {};

export const getElement = (selector) => {
	if (!memo[selector]) {
		memo[selector] = document.querySelector(selector);
	}
	return memo[selector];
};
