import movieDB  from 'themoviedb-javascript-library';
import compose from './../helpers/compose';

export const basicCall = (methodContainer, method, options) => {
	return new Promise((resolve, reject) => {

		movieDB[methodContainer][method](options, resolve, reject);
	});
};

export const defaultHandlers = (promise) => {
	return promise
		.then(resolveResponse => {
			return JSON.parse(resolveResponse);
		}, rejectResponse => {
			return JSON.parse(rejectResponse);
		});
};

export const movieDBCall = compose(defaultHandlers, basicCall);
