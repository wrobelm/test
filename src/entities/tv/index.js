import movieDB  from 'themoviedb-javascript-library';

export const api = {
	getTv: (query) => {
		return new Promise((resolve, reject) => {
			movieDB.search.getTv({query}, resolve, reject);
		});
	},
};
