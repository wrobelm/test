import movieDB  from 'themoviedb-javascript-library';

export const api = {
	getMovie: (query) => {
		return new Promise((resolve, reject) => {
			movieDB.search.getMovie({query}, resolve, reject);
		});
	},
};
