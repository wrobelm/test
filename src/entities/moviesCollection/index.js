import {movieDBCall} from './../../vendors/movieDB';

const model = {
	id: 0,
	backdrop_path: null,
	name: '',
	poster_path: null,
};

export const api = {
	getCollection: (query) => {
		return movieDBCall('search','getCollection', {query});
	},
};
