import {
	api,
} from './index';
import movieDB  from 'themoviedb-javascript-library';

import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';

chai.use(sinonChai);
chai.should();
const expect = chai.expect;
const assert = chai.assert;

describe('services', function () {

	describe('getCollection', () => {
		it('should resolve promise', () => {
			const expectedResponse = {
				page: 1,
				results: [{
					id: 86311,
					backdrop_path: '/zuW6fOiusv4X9nnW3paHGfXcSll.jpg',
					name: 'The Avengers Collection',
					poster_path: '/qJawKUQcIBha507UahUlX0keOT7.jpg',
				}],
				total_pages: 1,
				total_results: 1,
			};

			sinon.stub(movieDB.search, 'getCollection').returns(expectedResponse);

			api.getCollection('The%20Avengers%20Collection')
				.then((response) => {
					expect(response.page).to.equal(1);
				});
		});
	});
});
