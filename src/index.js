import movieDB  from 'themoviedb-javascript-library';
import {api_key} from './config/index';
import searchModule from './screens/search/index';

movieDB.common.api_key = api_key;

// here should be routing to fire controllers
// controllers should get data from routing/dispatcher
searchModule.controller.init();
