export default {
	query: '',
	selectedType: 'Collection',
	searchTypes: [{
		value: 'Collection',
		label: 'Collection',
	}, {
		value: 'Movie',
		label: 'Movie',
	}, {
		value: 'Tv',
		label: 'TV',
	}],
};
