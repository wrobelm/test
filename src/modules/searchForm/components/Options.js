import createElement from '../../../helpers/createElement';
import Option from './Option';

export default (options) => {
	const selectElement = createElement('select');

	options.forEach(optionElement => selectElement.append(Option(optionElement)));
	return selectElement;
};
