import createElement from '../../../helpers/createElement';
import model from '../model';
import Options from './Options';

export default () => {
	const searchForm = createElement('form');
	const searchInput = createElement('input');
	const searchSubmitButton = createElement('button');
	const searchTypeSelect = Options(model.searchTypes);

	searchSubmitButton.textContent = 'Search';
	searchSubmitButton.type = 'submit';

	// attach events
	searchInput.addEventListener('keyup', (e) => {
		model.query = e.target.value;
	}, false);
	searchTypeSelect.addEventListener('change', (e) => {
		model.selectedType = e.target.options[e.target.selectedIndex ].value;
	}, false);

	searchForm.append(searchInput);
	searchForm.append(searchTypeSelect);
	searchForm.append(searchSubmitButton);
	return searchForm;
};
