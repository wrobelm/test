import createElement from '../../../helpers/createElement';
import model from './../model';

export default (option) => {
	const optionElement = createElement('option');
	optionElement.value = option.value;
	optionElement.textContent = option.label;

	if (model.selectedType === option.value) {
		optionElement.selected = 'selected';
	}

	return optionElement;
};
