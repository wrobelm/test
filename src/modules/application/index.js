import {getElement} from '../../vendors/dom';

export const api = {
	getRegion() {
		return getElement('.app--region');
	},
};
