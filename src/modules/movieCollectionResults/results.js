export const resultTemplate = (result) => {

	const voteCount = result.vote_count?`Votes: ${result.vote_count} ` : '';
	const voteAverage = result.vote_average ? `Score: ${result.vote_average} ` : '';
	const title = result.title ? result.title : '';
	const name = result.name ? result.name : '';

	return `
	<li>${voteCount}${voteAverage}${title}${name}</li>
	`;
};

export const template = (results) => {
	return `
		<ul>
			${results.map(resultTemplate).join(' ')}
		</ul>
	`;
};
