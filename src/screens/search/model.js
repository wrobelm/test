import controller from './controller';

export default new Proxy({
	isLoading: false,
	results: [],
}, {
	set: (obj, prop, val)=> {
		obj[prop] = val;
		controller.onModelUpdate(obj, prop, val);
		return true;
	},
});
