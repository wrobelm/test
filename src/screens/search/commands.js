import {api as moviesCollectionEntity} from './../../entities/moviesCollection';
import model from './model';

export const getCollection = (query) => {
	model.isLoading = true;
	return new Promise((resolve, reject) => {
		moviesCollectionEntity.getCollection(query)
			.then((response)=> {
				model.isLoading = false;
				model.results = response.results;
			}, ()=>{

			});
	});
};
