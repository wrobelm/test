import createElement from '../../../helpers/createElement';

export const formRegion = createElement('div', 'form--region');
export const resultsRegion = createElement('div', 'results--region');

export const getTemplate = () => {
	const div = createElement('div');
	div.appendChild(formRegion);
	div.appendChild(resultsRegion);
	return div;
};
