import {getTemplate, formRegion, resultsRegion} from './components/Container';
import {api as applicationApi} from './../../modules/application';
import {Container as SearchFormContainer, model as searchModel} from '../../modules/searchForm';
import {getCollection} from './commands';
import {template as resultsTemplate} from './../../modules/movieCollectionResults/results';

// as there is no routing in this app than controller below will take care of  displaying things
const controller = {
	init() {
		controller.attachView();
	},

	attachView() {
		applicationApi.getRegion().appendChild(getTemplate());
		const SearchForm = SearchFormContainer();
		formRegion.appendChild(SearchForm);
		SearchForm.addEventListener('submit', controller.onSubmit, false);
	},

	onSubmit(e) {
		e.preventDefault();
		console.log(searchModel);
		getCollection(searchModel.query);
	},

	onModelUpdate(model, prop, val) {
		switch(prop) {
			case 'isLoading':
				resultsRegion.textContent = val ? 'Is loading' : '';
				break;
			case 'results':
				resultsRegion.innerHTML = resultsTemplate(val);
				break;
		}
	},
};
export default controller;
