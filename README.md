# pure es6 SPA application
Experiment to create modular es6 application where some parts can be replaced with some lib (react, redux, backbone, whatever)

## Quick start
Steps to run app:
1. npm install
1. go to src/config/index.js and set your api key
1. npm run start
1. open in browser http://localhost:3000 with working app

If you want to run tests run npm run dev:test

if you have another server working on port 3000 please change port in server.js file and run once again npm run start

## How to use

### tasks
1. start - run server and build production application
1. start:dev - run server, watchers for builders and livereload
1. server - run server
1. prod:build - build minimised JS
1. dev:build:js - build JS
1. dev:watch:js - set watcher and build JS on changes
1. dev:lint - check application code and fix what is possible
1. dev:livereload - run livereload
1. dev:test - run tests
1. dev:watch:test - set watcher and run tests on changes

### Structure

1. Screens are modules that are not shared and are responsible for displaying data to user. They use entities to get necessary data and modules to display this data.
2. Modules care only about displaying passed data. They can have own states (handled by models).
3. Entities can define models what are expected to be return by them. The main responsibility is to call (in)directly API to get data and return promise. Entities also could define their own stores that could be shared through application.
4. Vendors provide interfaces with outer world or abstract definitions (abstract models or views with predefined functionality, eg. method to connect application data with VDOM or IDOM).
5. Helpers are set of functions used through the application.
6. To do: router, but here lot of standalone libraries can be used.
7. Communication in application can be done in two ways: direct calls of functions (callbacks/actions) (flux way) or through events buses (cqrs way).

@TODO more tests
@TODO mocks automation with api-mock-proxy