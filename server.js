const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');

const app = express();
const hostname = process.env.HOSTNAME || 'localhost';
const port = parseInt(process.env.PORT, 10) || 3000;

app.use(express.static(__dirname+'/public'));
app.use(bodyParser.json());

app.all('/', (req, res) => {
	res.send(fs.readFileSync(__dirname + '/public/index.html', 'utf8'))
});

const server = app.listen(port, hostname, function () {
	const _host = server.address().address;
	const _port = server.address().port;

	console.log('App listening at http://%s:%s', _host, _port);
});
